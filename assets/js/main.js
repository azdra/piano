document.body.addEventListener("keydown", e =>  playSound(e.keyCode))
document.body.addEventListener("click", e => playSound(e.target.dataset.key))

const playSound = (code) => {
    let audio = document.querySelector(`audio[data-key="${code}"]`);
    let key = document.querySelector(`div[data-key="${code}"]`);

    if(!audio) return

    key.classList.toggle("playing")

    audio.currentTime = 0;
    audio.play();

    audio.onended = function() {
        key.classList.remove("playing")
    };
}